/**
 * Paper system implementations.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.system.inbuilt;
