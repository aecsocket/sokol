/**
 * Sokol Paper module, with implementations for the Paper platform.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper;
