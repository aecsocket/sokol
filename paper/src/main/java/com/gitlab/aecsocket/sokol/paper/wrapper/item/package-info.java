/**
 * Paper implementations of world wrappers - items.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.wrapper.item;
