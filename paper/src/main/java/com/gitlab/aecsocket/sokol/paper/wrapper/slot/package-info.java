/**
 * Paper implementations of item slots, such as in an inventory.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.wrapper.slot;
