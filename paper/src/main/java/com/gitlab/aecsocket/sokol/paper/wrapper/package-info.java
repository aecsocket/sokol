/**
 * Paper implementations of world wrappers.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.wrapper;
