/**
 * Paper implementations of item users, such as entities and players.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.wrapper.user;
