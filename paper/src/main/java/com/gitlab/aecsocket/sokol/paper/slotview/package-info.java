/**
 * Utilities for a GUI which shows the entire tree of a component tree, in an inventory, to users.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.slotview;
