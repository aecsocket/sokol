/**
 * Inbuilt Paper stat types.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.paper.stat;
