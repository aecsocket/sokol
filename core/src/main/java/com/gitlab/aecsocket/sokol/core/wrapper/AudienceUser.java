package com.gitlab.aecsocket.sokol.core.wrapper;

import net.kyori.adventure.audience.Audience;

public interface AudienceUser extends ItemUser, Audience {}
