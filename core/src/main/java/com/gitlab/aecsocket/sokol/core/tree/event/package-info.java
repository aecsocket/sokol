/**
 * Events concerning trees of component nodes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.tree.event;
