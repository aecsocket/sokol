/**
 * Interfaces for how an element of the world can be interacted with.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.wrapper;
