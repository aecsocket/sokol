/**
 * Systems that can be applied to components, and associated utilities.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.system;
