/**
 * Components, which serve as values for component tree nodes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.component;
