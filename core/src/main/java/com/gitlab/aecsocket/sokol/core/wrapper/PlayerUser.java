package com.gitlab.aecsocket.sokol.core.wrapper;

public interface PlayerUser extends AudienceUser {
    boolean sneaking();
    boolean sprinting();
    boolean leftHanded();

    boolean inAnimation();
}
