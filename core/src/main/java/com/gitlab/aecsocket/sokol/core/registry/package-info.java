/**
 * Storage of keyed objects, with a string ID.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.registry;
