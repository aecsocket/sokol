/**
 * Sokol core module.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core;
