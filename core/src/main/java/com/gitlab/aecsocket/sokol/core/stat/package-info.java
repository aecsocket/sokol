/**
 * Stats that serve the purpose of cross-system and cross-node communication.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.stat;
