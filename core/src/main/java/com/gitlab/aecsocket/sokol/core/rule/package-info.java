/**
 * Rules which can evaluate to true or false based on a provided component tree node.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.rule;
