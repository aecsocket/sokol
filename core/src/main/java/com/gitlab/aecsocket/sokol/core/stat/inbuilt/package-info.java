/**
 * Inbuilt stat types.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.stat.inbuilt;
