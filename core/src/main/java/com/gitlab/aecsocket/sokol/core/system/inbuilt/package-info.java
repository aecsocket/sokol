/**
 * Inbuilt system types, which must be implemented by a platform.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.sokol.core.system.inbuilt;
